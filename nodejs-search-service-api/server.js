const express = require('express')
const cors = require('cors');

require('dotenv').config()

// REDIS CLIENT & CONNNECTION START UP BLOCK
const redis = require('redis');
const redisOptions = {
    socket : {
        host: process.env.REDIS_SERVER,
        port: process.env.REDIS_PORT
    },
    retryStrategy: times => {
      // reconnect after
      return Math.min(times * 50, 2000);
    }
  };
const client = redis.createClient(redisOptions);
client.connect();
client.on('error', (err) => console.log('Redis Client Error', err));

//APP COMMON
const common = require('./common.js')

//SWAGGER DOCS
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

//AWS XRAY 

var AWSXRay = require('aws-xray-sdk');
AWSXRay.setContextMissingStrategy('IGNORE_ERROR');
AWSXRay.setDaemonAddress(process.env.XRAY_HOST_AND_PORT);

const app = express();
const port = 80;

app.use(cors());
app.use(express.json())
app.use(function(req, res, next) {
    req.redisClient = client;
    next();
});
app.use(function(req, res, next) {
    req.common = common;
    next();
});

if (process.env.NODE_ENV !='local') app.use(AWSXRay.express.openSegment('nodejs-search-public-api'));

//AWSXRay.middleware.enableDynamicNaming('*.example.com');

const documentsRouter = require('./routes/documents.js')
app.use('/documents', documentsRouter)

const indexesRouter = require('./routes/indexes.js');
app.use('/indexes', indexesRouter)


const { response } = require('express');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', (req, res) => {

    let segment = ''
    if (process.env.NODE_ENV !='local') segment = AWSXRay.getSegment(); //Event Tracing

    let result = {
        service : "search-service",
        description : "The service is build on Redis - RediSearch. \n\n Dokumentation:\nhttps://oss.redis.com/redisearch/  \n\nQuery reference: \nhttps://oss.redis.com/redisearch/Query_Syntax/"
    }
    res.json(result);   
});

app.use(AWSXRay.express.closeSegment());

app.listen(port, () => console.log(`Server listening on port ${port}!`));