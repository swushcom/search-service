#!/bin/bash
aws ecr get-login-password | docker login --username AWS --password-stdin 755526451691.dkr.ecr.eu-west-1.amazonaws.com
docker build -t nodejs-search-service-api .
docker tag nodejs-search-service-api:latest 755526451691.dkr.ecr.eu-west-1.amazonaws.com/nodejs-search-service-api:latest
docker push 755526451691.dkr.ecr.eu-west-1.amazonaws.com/nodejs-search-service-api:latest
UPDATED_TASK_DEFINITION=$(aws ecs register-task-definition --cli-input-json file://deploy-prod-nodejs-search-service-api-task-definition.json | jq-win64 '.taskDefinition.taskDefinitionArn' --raw-output)
echo $UPDATED_TASK_DEFINITION
aws ecs update-service --service nodejs-search-service-api--cluster services --task-definition ${UPDATED_TASK_DEFINITION}



#aws ecs update-service --service nodejs-search-service-api --cluster services --task-definition arn:aws:ecs:eu-west-1:755526451691:task-definition/nodejs-search-service-api:7