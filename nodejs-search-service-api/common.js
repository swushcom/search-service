
const redisToJson = (input,process = ['']) => {
    if (process[0] != '' ) process.unshift('')
    for (i=0;i< process.length;i++) {
        input = redisToJsonPart(input, process[i])
    }
    return input
}

const redisToJsonPart = (input,statingFrom = '',hasTitle= false) => {
  
    let processPart = ''
    let object = {}        

    if (statingFrom != '' && input[statingFrom]) { 
        //TODO - TEST at statingFrom eksisterer ellers spring over. 
        //if (input[statingFrom] ) { 
            processPart = input[statingFrom]
        //}
    } 
    else { processPart = input }

    for (let i = 0; i < processPart.length; i = i+2) {
        object[processPart[i]] = processPart[i+1]
    }

    if (statingFrom != '') { input[statingFrom] = object } 
    else {  input = object }

    return input
}


module.exports = {
    redisToJson,
    redisToJsonPart
};