const express = require('express');
const router = express.Router();
const redis = require('redis');
const searchCommon = require('../common.js')
require('dotenv').config()

const redisOptions = {
  socket : {
      host: process.env.REDIS_SERVER,
      port: process.env.REDIS_PORT
  },
  retryStrategy: times => {
    // reconnect after
    return Math.min(times * 50, 2000);
  }
};


  router.get('/index/:INDEX', async (req, res) => {
      console.log('#########################################################')
      const client = redis.createClient(redisOptions);
      client.on('error', (err) => console.log('Redis Client Error', err));
      await client.connect();

      //let query = req.query.q + '@network:' + req.headers.network
      let query = req.query.q    
      let next = req.query.next | 0 
      let limit = req.query.limit | 1

      let command = ['FT.SEARCH', 'SF_COMP', query, 'LIMIT', next.toString(), limit.toString()]
      let searchData = await client.sendCommand(command)
  
      let numOfResults = searchData.slice(0,1)[0]
      let results = searchCommon.redisToJson(searchData.slice(1))

      let returnArray = []
      for (const [key, value] of Object.entries(results)) {
          values = searchCommon.redisToJson(value)
          returnArray.push(
              { key : key, prefix: key.split(':')[0], id: key.split(':')[1], values }
          )
      }

      res.json( { numOfResults, results : returnArray })
  })

module.exports = router;
