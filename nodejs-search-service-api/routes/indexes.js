const express = require('express');
const router = express.Router();
//const redis = require('redis');
require('dotenv').config()

var AWSXRay = require('aws-xray-sdk');

//Getting all indexes
router.get('/', async (req, res) => {

    let segment = ''
    if (process.env.NODE_ENV !='local') segment = AWSXRay.getSegment(); //Event Tracing

    if (process.env.NODE_ENV !='local') var subSeg = segment.addNewSubsegment('Redis Command - FT._List');
    let indexesList = await req.redisClient.sendCommand(['FT._List'])
    if (process.env.NODE_ENV !='local') subSeg.close()

    if (process.env.NODE_ENV !='local') var subSegC = segment.addNewSubsegment('Send response - Object:indexesList');
    res.json(indexesList)
    if (process.env.NODE_ENV !='local') subSegC.close()
   
})

//Get one index
router.get('/:idx', async (req, res) => {

    let segment = ''
    if (process.env.NODE_ENV !='local') segment = AWSXRay.getSegment(); //Event Tracing

    if (process.env.NODE_ENV !='local') var subSegB = segment.addNewSubsegment('JS Function - loadIndexInfo');
    if (process.env.NODE_ENV !='local') subSegB.addMetadata('IndexKey', req.params.idx)
    let result = await loadIndexInfo(req.params.idx,req.redisClient,req.common)
    if (process.env.NODE_ENV !='local') subSegB.close()

    if (process.env.NODE_ENV !='local') var subSegC = segment.addNewSubsegment('Send response - Object:result');
    res.json(result)
    if (process.env.NODE_ENV !='local') subSegC.close()
})


//Qyery index
router.get('/:idx/query', async (req, res) => {
    

    let segment = ''
    if (process.env.NODE_ENV !='local') segment = AWSXRay.getSegment(); //Event Tracing

    try {
        
        let query = req.query.q    
        let limit = req.query.pageSize ? req.query.pageSize : 10        
        let page =  (req.query.page && req.query.page > 0) ? parseInt(req.query.page) : 1
        let after = (page * limit) - limit
        let sortBy = req.query.sort ? req.query.sort : -1

        // if q is missing Throw Error
        if (typeof query == 'undefined') throw new Error('Required parameter [q] is missing') 


        if (process.env.NODE_ENV !='local') var subSegA = segment.addNewSubsegment('Redis - FT.SEARCH Command');
        let command = ['FT.SEARCH', req.params.idx, query, 'LIMIT', after.toString(), limit.toString()]

        // IF SORT
        if (sortBy != -1) { 
            let direction = 'ASC'
            if (sortBy.split(' ').length > 1) {
                let key = sortBy.split(' ')
                sortBy = key[0]
                direction = key[1]
            }
            command = [...command, 'SORTBY', sortBy, direction]
        }

        if (process.env.NODE_ENV !='local') subSegA.addMetadata('SearchQueryCommand', command)
        let searchData = await req.redisClient.sendCommand(command)
        if (process.env.NODE_ENV !='local') subSegA.close()

        
        if (process.env.NODE_ENV !='local') var subSegB = segment.addNewSubsegment('Build and send response');
        let count = searchData.slice(0,1)[0]
        let result = req.common.redisToJson(searchData.slice(1))
        let returnArray = []
        for (const [key, value] of Object.entries(result)) {
            values = req.common.redisToJson(value)
            returnArray.push(
                //{ "_cursor" : key, id: key.split(':')[1], prefix: key.split(':')[0], ...values }
                { "_cursor" : key, id: key.split(':')[2], ...values }
            )
        }                
        res.json( { count, page, pageSize : parseInt(limit), itemsCount : returnArray.length, items : returnArray })
        if (process.env.NODE_ENV !='local') subSegB.close();

    }
    catch (Err) {
        //console.log(Err)
        res.status(404).send({"error": Err.toString() })
        throw Err
    
    }
/*
    if (Err) {
        next(Err) // Pass errors to Express.
    }*/
   
})

const loadIndexInfo = async (idx, redisClient, common) => {

    //let r = indexesList[i]
    try {
        let command = ['FT.Info', idx]
        let indexInfo = await redisClient.sendCommand(command)
        indexInfo = common.redisToJson(indexInfo,['index_definition','gc_stats','cursor_stats'])
        
        let newItem = { 
            key : indexInfo.index_name,
            numOfDocs : parseInt(indexInfo.num_docs),
            numOfRecords : parseInt(indexInfo.num_records),
            isIndexing : indexInfo.indexing,
            percentIndexed : indexInfo.percent_indexed,
        }

        if (indexInfo.fields.length > 0) {
            newItem.fields = indexInfo.fields.map( (item) => { 
                item.unshift('name')
                item = common.redisToJsonPart(item)
                return item 
            })
        }
        
        return newItem
    }
    catch (Err) {
        throw Err
    }

}

module.exports = router;


/*


*/