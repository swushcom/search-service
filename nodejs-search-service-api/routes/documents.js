const express = require('express');
const router = express.Router();
require('dotenv').config()

const AWS = require('aws-sdk');
const fetch = require('node-fetch')
var AWSXRay = require('aws-xray-sdk');

//Creating one or more documents. Will overwrite document if key exist.
router.post('/threaded', async (req, res) => {

    let segment = ''
    if (process.env.NODE_ENV !='local') segment = AWSXRay.getSegment(); //Event Tracing

    try {
            
        let bodySets = []
        let pathSets = []
        let s3Sets = []

        if (req.body.sets) {
            bodySets = req.body.sets
        }
        //console.log('bodySets - First >>>>> ',req.body.sets)

        if (req.body.path) {
            const fetchOptions = {
                method: 'GET'
            }
            if (process.env.NODE_ENV !='local') var sub = seg.addNewSubsegment('fetch - load data');
            if (process.env.NODE_ENV !='local') sub.addMetadata('Url', req.body.path)
            if (process.env.NODE_ENV !='local') sub.addMetadata('Fetch options', fetchOptions)

            let data = await fetch(req.body.path,fetchOptions)
            pathSets = await data.json()

            if (process.env.NODE_ENV !='local') sub.close();
        }

        if (req.body.s3objects) {

           
            var s3 = new AWS.S3();
            //AWS.config.update( { region : 'eu-west-1'})
         
           // for (let i=0; i<req.body.s3objects.length;i++){

            //let key = eq.body.s3objects
            let params = {
                Bucket: "microservices-internal", 
                Key: "search/intake/" + req.body.s3objects
            }
            try {
                let data = await s3.getObject(params).promise();
                let body = data.Body.toString('utf-8')
                s3Sets = JSON.parse(body)    
                //s3Sets = body    
                }
            catch(err) {
                console.log(err)
                throw err
            }
            //}

            //s3Sets = body 
            //return false
         
            if (process.env.NODE_ENV !='local') sub.close();
        }

        let allSets = [...bodySets, ...pathSets, ...s3Sets]

        let startDate = new Date()
        let maxThreads = allSets.length < 200 ? 1 : 100
        //console.log(maxThreads, allSets.length)
        //if (allSets.length) < 100 

        let tasksPerThread = parseInt(allSets.length/maxThreads) +1

        if (allSets.length > 0) { 
            
            var promises = [];

            for (i = 0; i < maxThreads; i++) { 
                let startpos = i*tasksPerThread
                let endpos = tasksPerThread+(i*tasksPerThread)
                let thredTasks = allSets.slice(startpos,endpos)  
                //console.log(thredTasks)
                promises.push(ThreadWorker(thredTasks,i,req.redisClient))     
            }

            //console.log(packagedTasks)

            try {
                if (process.env.NODE_ENV !='local') var sub2 = seg.addNewSubsegment('Redis - HSET ');
                result = await Promise.all(promises);
    

                let endDate = new Date()
                let diff = endDate - startDate
                let docPerSec = parseInt(allSets.length / (diff/1000))

                if (process.env.NODE_ENV !='local') sub2.addMetadata('docs/sec', docPerSec)
                if (process.env.NODE_ENV !='local') sub2.addMetadata('docs added/updated', allSets.length)

                if (process.env.NODE_ENV !='local') sub2.close();

                console.log('Redis Success >>');
            } catch (err) {
                console.log('Redis Error >>', err);
            }
    

        }

        let endDate = new Date()
        let diff = endDate - startDate
        let docPerSec = parseInt(allSets.length / (diff/1000))
        
        let response = { 
            documentsAdded : allSets.length,
            processTimeMs : diff,
            processTimeSec : diff/1000,
            documentsPerSec : docPerSec
        }

        res.json(response)

        return true
    }
    catch (Err) {

        //console.log(Err)
        res.status(404).send({"error": Err.toString() })
        throw Err
    
    }
})


const ThreadWorker = async (tasks,name,redisClient) => {
    for (let i = 0; tasks.length > i; i++) {  
        let { key, ...set } = tasks[i]  
      
        let a = await redisClient.HSET(key, set)
        //console.log(a)
    }
}


router.delete('/', async (req, res) => {

    let bodySets = ''
    let pathSets = ''
    if (req.body.sets) {
        bodySets = req.body.sets
    }
    if (req.body.path) {
        const fetchOptions = {
            method: 'GET'
        }
        let data = await fetch(req.body.path,fetchOptions)
        console.log('loading from path')
        pathSets = await data.json()
    }

    let allSets = [...bodySets, ...pathSets]
    let result = ''
    if (allSets.length > 0) {
        const client = redis.createClient(redisOptions);
        client.on('error', (err) => console.log('Redis Client Error', err));
        await client.connect();
        result = await client.DEL(allSets)

    }
    res.json({ statusCode : 200, body : "Deleted " + result + ' keys.'})
 
})

module.exports = router;


/*

FT.CREATE SF_COMP ON HASH PREFIX 1 SF_COMP: SCHEMA name TEXT WEIGHT 5.0 gameId TEXT subsiteKey TEXT ownerName TEXT enrolled NUMERIC hasPassword NUMERIC 


hset SF_COMP:23423443 name "Swush.com Dyster" description "lorem ipsum" ownerName "Jesper - Swush.com" ownerKey "jesper_swush_com"
hset SF_COMP:34534554 name "Swushiii" description "Gid man arbejdede hos Swush" ownerName "Peter Hansen" ownerKey "peter_hansen"
hset SF_COMP:34534554 name "Swushiii" description "Gid man arbejdede hos Swush" ownerName "Peter Hansen" ownerKey "peter_hansen"





// INDEX FOR EVERYTHING
FT.CREATE SF ON HASH PREFIX 4 "COMP:" "FAN_USR:" "FAN_TEAM:" "FORUM_POST:" SCHEMA type TAG scope TAG title TEXT WEIGHT 5.0 body TEXT game_id TEXT enrolled NUMERIC has_password NUMERIC owner_title TEXT owner_key TEXT created TEXT culture_key TEXT


                   FAN_USR:FAN_TEAM:FORUM_POST:       


*/