const swushdata = require('swushdata')
const swushhelpers = require('swushhelpers')
require('dotenv').config()

const AWS = require('aws-sdk')
AWS.config.update({region: 'eu-west-1'});

var s3 = new AWS.S3();

exports.handler = async (event) => {

    const SQLServerConfig = {
        user: process.env.secretSQLUser,
        password: process.env.secretSQLPassword,
        server: process.env.secretSQLServer,
        database: process.env.secretSQLDatabase,
        secure: true,
        options : { 
            enableArithAbort : true,
            trustServerCertificate: true
        }
    }

    let result = ''

    try {     

        let sqlQuery = await swushdata.buildQuery('./functions/season-full-dump/query-export.sql',[])
        let [users, competitions, fantasyTeams, forumPosts] = await swushdata.runQuery(sqlQuery, SQLServerConfig);

        users = users.map((item) => {
            item.type = 'fan_usr'; item.key = item.type + ':' + item.key 
            return item
        })
        await swushhelpers.s3.putObject('microservices-internal', 'search/intake/full_dump_users.json',JSON.stringify(users),false,'text/json')

        competitions = competitions.map((item) => {
            item.type = 'comp' ; item.key = item.type + ':' + item.key
            item.owner_key = 'fan_usr:' + item.owner_key
            item.game_id = item.game_id.toString()
            return item
        })
        await swushhelpers.s3.putObject('microservices-internal', 'search/intake/full_dump_competitions.json',JSON.stringify(competitions),false,'text/json')

        fantasyTeams = fantasyTeams.map((item) => {
            item.type = 'fan_team' ; item.key = item.type + ':' + item.key
            item.owner_key = 'fan_usr:' + item.owner_key
            return item
        })
        await swushhelpers.s3.putObject('microservices-internal', 'search/intake/full_dump_fantasy_teams.json',JSON.stringify(fantasyTeams),false,'text/json')

        forumPosts = forumPosts.map((item) => {
            item.type = 'forum_post' ; item.key = item.type + ':' + item.key
            item.owner_key = 'fan_usr:' + item.owner_key
            return item
        })
        await swushhelpers.s3.putObject('microservices-internal', 'search/intake/full_dump_forum_posts.json',JSON.stringify(forumPosts),false,'text/json')

       //let full = [...users, ...competitions, ...fantasyTeams, ...forumPosts ]

       //let sqlQueryResult = await RunSQLquery(sqlQuery,SQLServerConfig); 
       //let a = await swushhelpers.s3.putObject('microservices-internal', 'search/intake/full_dump.json',JSON.stringify(full),false,'text/json')
       //console.log(a)

        result = { result : 'OK!'}
        return ({
            statusCode: 200,
            body: JSON.stringify(result),
            headers : {
                "Content-Encoding": 'application/json'
            }
        })
        //result = sqlQueryResult   
    }
    catch(err) {
        //console.log('No such function')
        return err
        result.statusCode = 400
        result.body = err
    }

    //return result



    return ({
        statusCode: 200,
        body: JSON.stringfy(result),
        headers : {
            "Content-Encoding": 'text:json'
        }
    })

    
}

const RunSQLquery = async (SQLString,databaseConfig) => new Promise((resolve, reject) => {
    

    (async function () {
        try {
            let pool = await sql.connect(databaseConfig)
            let result = await pool.request()
                .query(SQLString)

            sql.close();

            if (result.recordsets.length > 1) {
                resolve(result.recordsets);
            } else {
                resolve(result.recordsets[0]);
            }
            
        } catch (err) {
            // ... error checks
            sql.close()
            reject(err)
        }
    })()

});