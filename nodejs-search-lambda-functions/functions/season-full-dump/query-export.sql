--use swushcore

-- FANTASY USER
	SELECT TOP 10
		u.id as [key], 
		ss.[Key] as scope, 
		--'FAN_USR' as [type],
		u.[name] as [title], 

		left(CONVERT(nvarchar, U.CreatedUtc, 126),19) + 'Z'as created
	FROM [User] U
		inner join [Subsites] ss on ss.Id = U.SubsiteId
	WHERE U.IsActivated = 1


-- COMPETITION
	SELECT TOP 10
		c.id as [key],
		ss.[Key] as [scope],
		--'COMP' as [type],

		c.name as [title], 
		c.gameid as game_id,
		CASE 
			WHEN len(C.Password) > 0 THEN 1
			ELSE 0
		END as hasPassword,		
		u.[name] as owner_title, 
		u.id as owner_key, 
		c.EnrollmentCount as enrolled,
		left(CONVERT(nvarchar, C.CreatedUtc, 126),19) + 'Z'as created
	FROM Competition C 	
		inner join [user] u on c.OwnerUserId = u.Id
		inner join [Subsites] ss on ss.Id = C.SubsiteId
		WHERE c.isDeleted <> 1



-- FANTASY_TEAM
	Select TOP 10
		ut.id as [key], 
		ss.[Key] as [scope],
		ut.[name] as [title], 
		ss.[Key] as scope, 
		ut.gameid as game_id, 
		u.id as owner_key,
		u.name as owner_title,
		left(CONVERT(nvarchar, UT.CreatedUtc, 126),19) + 'Z'as created

	from [UserTeam] UT	
		inner join [User] u on UT.userid = u.Id
		inner join [Subsites] ss on ss.Id = U.SubsiteId
	WHERE UT.Status = 2
			AND U.isDeleted = 0
			AND U.isActivated = 1

-- FORUM_POST
	Select TOP 10
		P.id as [key], 
		ss.[Key] as scope,	
		--'FORUM_POST' as [type]
		--P.parentid as ParentId,

		P.[subject] as [title], 
		P.[body] as [body],	
		u.[name] as [owner_title], 
		u.id as owner_key,
		left(CONVERT(nvarchar, p.CreatedUtc, 126),19) + 'Z'as created
	from Post P
		inner join [User] u on P.UserId = u.Id
		inner join [Subsites] ss on ss.Id = U.SubsiteId
		inner join PublicForum PF on PF.ForumId = P.ForumId
	WHERE U.IsActivated = 1
		and p.CreatedUtc > '2018-01-01 00:00'
		and pf.SubsiteId in (6,7)
	order by P.id desc


/*
select top 10 * from post
select * from PublicForum
*/
/*

select top 10 c.id, c.gameid, c.[name], u.[name] as ownerName, u.id as userKey, c.EnrollmentCount from Competition C
	inner join [user] u on c.OwnerUserId = u.Id

select id, name from game order by id desc


*/


/*
		
		################  INDEXED TYPES  ####################

		
		FANTASY USER
        { 
			"key":"FAN_USR:1",
			"type" :"SF_USR",
			"scope":"holdet",
			"title": "Jesper - Swush.com",
			"created": "2003-01-05T11:41:14Z" 
		}

		COMPETITION
		{ 
			"key": "COMP:6160",
			"type":"COMP",
			"scope":"holdet",
			"title": "Swush EURO 2020 Duellen",
			"hasPassword":1,
			"game_id": "143",
			"enrolled": 1,
			"owner_title": "Jesper - Swush.com",
			"owner_key":"FAN_USR:1",
			"created": "2003-01-05T11:41:14Z"
		}

		FANTASYTEAM
        { 
			"key":"FAN_TEAM:1",						//TEXT NOINDEX
			"type" :"FAN_TEAM",						//TAG
			"scope":"holdet",						//TAG
			"game_id": "543",						//TEXT NOINDEX	
			"title": "Swush FC United",				//TEXT INDEX
			"owner_title": "Jesper- Swush.com",			//TEXT INDEX
			"owner_key":"FAN_USR:1",					//TEXT INDEX
			"created": "2003-01-05T11:41:14Z"		//TEXT INDEX
		}

		FORUM POST
        { 
			"key":"FORUM_POST:1", 
			"type" :"FORUM_POST",
			"scope":"holdet",
			"title":"Skal vi have en Swush miniliga",
			"body":"Skal vi have en lille Swush konkurrence\n Det må snart være tid til at vi sætter noget på højkant så vi kan se hvem der er den bedte til EM. \n\n Kom så!" ,
			"owner_title": "Jesper - Swush.com",
			"owner_key":"FAN_USR:1",
			"culture_key: "da-DK",
			"created": "2003-01-05T11:41:14Z"
			
		}
   
   
	type TAG 
	scope TAG 

	title TEXT WEIGHT 5.0 
	body TEXT 

	game_id TEXT 
	enrolled NUMERIC 
	has_password NUMERIC 

	owner_title TEXT 
	owner_key TEXT 
	created TEXT
	culture_key TEXT

   
   
   */


