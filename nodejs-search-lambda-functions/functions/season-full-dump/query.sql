

-- USER
SELECT '{ "key": "FAN_USER:'+ cast(id as nvarchar) +'", "type":"FAN_USER", "scope":"'+scope+'", "title":"'+replace(replace([name],'"',''),'\','\\') +'", "created":"'+CreatedUtc+'" },' as obje
FROM  (
		select TOP 100 
			u.id, u. [name] as [name], ss.[Key] as scope, left(CONVERT(nvarchar, U.CreatedUtc, 126),19) + 'Z'as CreatedUtc
		from [User] U
			inner join [Subsites] ss on ss.Id = U.SubsiteId
		WHERE U.IsActivated = 1
			--AND c.gameid = 543
) as dtbl


-- COMPETITION
SELECT '{ "key": "COMP:'+ cast(id as nvarchar) +'", "type":"COMP", "scope":"'+scope+'", "has_password":'+ hasPassword+', "game_id": "'+ gameid +'", 
		"owner_key":"SF:USR:'+ cast(userKey as nvarchar) +'", 
"title": "'+ CompName +'", "owner_name": "'+ ownerName +'", "enrolled": '+ EnrollmentCount +',"created":"'+CreatedUtc+'" },' as obje
FROM  (
		select TOP 100 c.id as id, cast(c.gameid as nvarchar) as gameid, replace(replace(c.name,'"',''),'\','\\') as CompName, replace(replace(u.[name],'"',''),'\','\\') as ownerName, 
		u.id as userKey, cast(c.EnrollmentCount as nvarchar) as EnrollmentCount, ss.[Key] as scope, left(CONVERT(nvarchar, C.CreatedUtc, 126),19) + 'Z'as CreatedUtc,
			CASE 
				WHEN len(C.Password) > 0 THEN '1'
				ELSE '0'
			END as hasPassword
		from Competition C 	
			inner join [user] u on c.OwnerUserId = u.Id
			inner join [Subsites] ss on ss.Id = C.SubsiteId
			WHERE c.isDeleted <> 1
	--where c.gameid = 543
) as dtbl


-- FANTASY_TEAM
SELECT '{ "key": "FAN_TEAM:'+ cast(utid as nvarchar) +'", "type":"FAN_TEAM", "scope":"'+scope+'", "title":"'+[fantasyteamName]+'", "game_id":"'+ cast(gameid as nvarchar)+'", "owner_title","'+[owner]+'", "owner_key","FAN_USR:'+ cast(ownerId as nvarchar)+'", "created":"'+CreatedUtc+'" },' as obje
FROM  (

		Select TOP 100 
			ut.id as utid, ut. [name] as [fantasyteamName], ss.[Key] as scope, ut.gameid, 
			u.id as ownerId, u.name as [owner],
			left(CONVERT(nvarchar, UT.CreatedUtc, 126),19) + 'Z'as CreatedUtc
		from [UserTeam] UT	
			inner join [User] u on UT.userid = u.Id
			inner join [Subsites] ss on ss.Id = U.SubsiteId
		WHERE UT.Status = 2
			  AND U.isDeleted = 0
			  AND U.isActivated = 1
	--where c.gameid = 543
) as dtblFantasyTeam


-- FORUM_POST
SELECT '{ "key": "FORUM_POST:'+ cast(postId as nvarchar) +'", "type":"FORUM_POST", "scope":"'+scope+'", "title":"'+[title]+'", 
		"body":"'+ [body] +'",
		"owner_title" : "",
		"created":"'+CreatedUtc+'" },' as obje
FROM  (
		Select TOP 100 
			P.id as postId, P.parentid as ParentId, replace(replace(u.[name],'"',''),'\','\\') as [owner_title], 'FAN_USR:' + cast(u.id as nvarchar) as owner_key, ss.[Key] as scope,	P.[subject] as [title], replace(replace(P.[body],'"',''),'\','\\')  as [body],	left(CONVERT(nvarchar, p.CreatedUtc, 126),19) + 'Z'as CreatedUtc
		from Post P
			inner join [User] u on P.UserId = u.Id
			inner join [Subsites] ss on ss.Id = U.SubsiteId
			inner join PublicForum PF on PF.ForumId = P.ForumId
		WHERE U.IsActivated = 1
			and p.CreatedUtc > '2018-01-01 00:00'
			and pf.SubsiteId in (6,7)
		order by P.id desc
) as dtbl

/*
select top 10 * from post
select * from PublicForum
*/
/*

select top 10 c.id, c.gameid, c.[name], u.[name] as ownerName, u.id as userKey, c.EnrollmentCount from Competition C
	inner join [user] u on c.OwnerUserId = u.Id

select id, name from game order by id desc


*/


/*
		
		################  INDEXED TYPES  ####################

		
		FANTASY USER
        { 
			"key":"FAN_USR:1",
			"type" :"SF_USR",
			"scope":"holdet",
			"title": "Jesper - Swush.com",
			"created": "2003-01-05T11:41:14Z" 
		}

		COMPETITION
		{ 
			"key": "COMP:6160",
			"type":"COMP",
			"scope":"holdet",
			"title": "Swush EURO 2020 Duellen",
			"hasPassword":1,
			"game_id": "143",
			"enrolled": 1,
			"owner_title": "Jesper - Swush.com",
			"owner_key":"FAN_USR:1",
			"created": "2003-01-05T11:41:14Z"
		}

		FANTASYTEAM
        { 
			"key":"FAN_TEAM:1",						//TEXT NOINDEX
			"type" :"FAN_TEAM",						//TAG
			"scope":"holdet",						//TAG
			"game_id": "543",						//TEXT NOINDEX	
			"title": "Swush FC United",				//TEXT INDEX
			"owner_title": "Jesper- Swush.com",			//TEXT INDEX
			"owner_key":"FAN_USR:1",					//TEXT INDEX
			"created": "2003-01-05T11:41:14Z"		//TEXT INDEX
		}

		FORUM POST
        { 
			"key":"FORUM_POST:1", 
			"type" :"FORUM_POST",
			"scope":"holdet",
			"title":"Skal vi have en Swush miniliga",
			"body":"Skal vi have en lille Swush konkurrence\n Det må snart være tid til at vi sætter noget på højkant så vi kan se hvem der er den bedte til EM. \n\n Kom så!" ,
			"owner_title": "Jesper - Swush.com",
			"owner_key":"FAN_USR:1",
			"culture_key: "da-DK",
			"created": "2003-01-05T11:41:14Z"
			
		}
   
   
	type TAG 
	scope TAG 

	title TEXT WEIGHT 5.0 
	body TEXT 

	game_id TEXT 
	enrolled NUMERIC 
	has_password NUMERIC 

	owner_title TEXT 
	owner_key TEXT 
	created TEXT
	culture_key TEXT

   
   
   */


