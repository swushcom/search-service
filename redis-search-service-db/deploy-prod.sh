#!/bin/bash
aws ecr get-login-password | docker login --username AWS --password-stdin 755526451691.dkr.ecr.eu-west-1.amazonaws.com
docker build -t redis-search-service-db .
docker tag redis-search-service-db:latest 755526451691.dkr.ecr.eu-west-1.amazonaws.com/redis-search-service-db:latest

#repository must exist
docker push 755526451691.dkr.ecr.eu-west-1.amazonaws.com/redis-search-service-db:latest
UPDATED_TASK_DEFINITION=$(aws ecs register-task-definition --cli-input-json file://deploy-prod-redis-search-service-db-task-definition.json | jq-win64 '.taskDefinition.taskDefinitionArn' --raw-output)
echo $UPDATED_TASK_DEFINITION
aws ecs update-service --service redis-search-service-db --cluster services --task-definition ${UPDATED_TASK_DEFINITION}

#aws ecs update-service --service redis-search-service-db --cluster services --task-definition arn:aws:ecs:eu-west-1:755526451691:task-definition/redis-search-service-db:3



#alias jq=/windows/system32/jq-win64.exe